#include "disass-lib.h"


//Used to search for a function definition in a file
int search_for_function(FILE *f, char *string) {
    // Read the file byte by byte
    char byte;
    while ((byte = fgetc(f)) != EOF) {
      registers[6]++;
       // If we find the start of a function definition
       if (byte == 0x01) {
          // Read the size of the function name
           char size = fgetc(f);
           registers[6]++;
          // Read the function name
          char function_name[size + 1];
          fread(function_name, 1, size, f);
          for (int i = 0; i < size; i++)
          {
            registers[6]++;
          }
          function_name[size] = '\0';
          
          if (fgetc(f) == 0x02){
            registers[6]++;
            // If the function name matches the string we are searching for
            if (strcmp(function_name, string) == 0) {
               // Return the next byte, which is the start of the function body
               temp_adress = registers[6];
               return ftell(f);
            }
          }
       }
    }

    // If we reach the end of the file without finding the function
    return -1;
}


int get_right_register_index(int x)
{
    if ( x >= 0 && x <= 5){
        return x;
    }
    else if (x == 15){
        return 6;
    }
    else if (x == 18){
        return 7;
    }
    else{
        return -1;
    }
}


void vm_jump(FILE *f,  char byte) {
    registers[6]++;
    // Read the offset of the jump destination
    char offset = fgetc(f);
    registers[6]++;
    // If the register 0 is not equal to 0, then we need to skip over the offset
    if (registers[0] != 0 && offset > 0) {
       for (int i = 0; i < offset - 1; i++){
          fgetc(f);
          registers[6]++;
       }
    } else {
       for (int i = 0; i < 3; i++) {
          fgetc(f);
          registers[6]++;
       }
    }

    printf("%d : jnz %d\n", temp_adress, offset+registers[6]);
    
    temp_adress = registers[6];
}

void vm_movc(FILE *f,  char byte) {
    // Read the register index of the destination register
    char register_index = fgetc(f) - 0x41;
    
    registers[6]++;

    // Read the value to store in the register
    char value = fgetc(f);

    registers[6]++;

    // Store the value in the register
    registers[get_right_register_index(register_index)] = value;
    printf("%d : movc $%d, #0x%x\n", temp_adress, get_right_register_index(register_index), value);
    fgetc(f);
    registers[6]++;
    fgetc(f);
    registers[6]++;
    fgetc(f);
    temp_adress = registers[6];
}

void vm_andl(FILE *f,  char byte) {
    // Read the register indices of the two operands
    registers[6]++;
    char register_index_1 = fgetc(f) - 0x41;
    registers[6]++;
    char register_index_2 = fgetc(f) - 0x41;
    registers[6]++;
    // Get the values from the registers
    char value_1 = registers[get_right_register_index(register_index_1)];
    char value_2 = registers[get_right_register_index(register_index_2)];

    // Perform the AND operation
    char result = value_1 & value_2;

    printf("%d : andl $%d, $%d\n", temp_adress, get_right_register_index(register_index_1), get_right_register_index(register_index_2));

    // Store the result in the first register
    registers[get_right_register_index(register_index_1)] = result;

    temp_adress = registers[6];
}

void vm_puts(FILE* f, char byte){
    registers[6]++;
    printf("%d : puts $0\n", temp_adress);
    temp_adress = registers[6];
}

void vm_getline(FILE* f, char byte){
    registers[6]++;
    printf("%d : getline\n", temp_adress);
    temp_adress = registers[6];
}

void vm_call(FILE* f, char byte){
    registers[6]++;
    char arg1 = fgetc(f);
    printf("%d : call ", temp_adress);
    for (int i = 0; i < arg1; i++){
        printf("%c", fgetc(f));
        registers[6]++;
    }
    printf("\n");
}


void vm_ret(FILE* f, char byte){
    printf("ret \n");
    registers[6]++;
}

void vm_add(FILE *f,  char byte) {
    // Read the register indices of the two operands
    registers[6]++;
    char register_index_1 = fgetc(f) - 0x41;
    registers[6]++;
    char register_index_2 = fgetc(f) - 0x41;
    registers[6]++;
    // Get the values from the registers
    char value_1 = registers[get_right_register_index(register_index_1)];
    char value_2 = registers[get_right_register_index(register_index_2)];

    // Perform the ADD operation
    char result = value_1 + value_2;

    printf("%d : add $%d, $%d\n", temp_adress, get_right_register_index(register_index_1), get_right_register_index(register_index_2));

    // Store the result in the first register
    registers[get_right_register_index(register_index_1)] = result;
    temp_adress = registers[6];
}

void vm_sub(FILE *f,  char byte) {
    // Read the register indices of the two operands
    registers[6]++;
    char register_index_1 = fgetc(f) - 0x41;
    registers[6]++;
    char register_index_2 = fgetc(f) - 0x41;
    registers[6]++;
    // Get the values from the registers
    char value_1 = registers[get_right_register_index(register_index_1)];
    char value_2 = registers[get_right_register_index(register_index_2)];

    // Perform the AND operation
    char result = value_1 - value_2;

    printf("%d : sub $%d, $%d\n", temp_adress, get_right_register_index(register_index_1), get_right_register_index(register_index_2));

    // Store the result in the first register
    registers[get_right_register_index(register_index_1)] = result;

    temp_adress = registers[6];
}


void vm_xorl(FILE *f,  char byte) {
    // Read the register indices of the two operands
    registers[6]++;
    char register_index_1 = fgetc(f) - 0x41;
    registers[6]++;
    char register_index_2 = fgetc(f) - 0x41;
    registers[6]++;
    // Get the values from the registers
    char value_1 = registers[get_right_register_index(register_index_1)];
    char value_2 = registers[get_right_register_index(register_index_2)];

    // Perform the AND operation
    char result = value_1 ^ value_2;

   printf("%d : xorl $%d, $%d\n", temp_adress, get_right_register_index(register_index_1), get_right_register_index(register_index_2));

    // Store the result in the first register
    registers[get_right_register_index(register_index_1)] = result;
    temp_adress = registers[6];
}

void vm_exit_routine(FILE* f, char byte){
    printf("exit_routine\n");
    registers[6]++;
}

void vm_inc_ic(FILE* f, char byte){
    printf("inc_ic\n");
    registers[6]++;
}

void vm_fopen(FILE* f, char byte){
    printf("fopen\n");
    registers[6]++;
}

void vm_mov(FILE* f, char byte){
    // Read the register indices of the two operands
    registers[6]++;
    int arg1 = fgetc(f);
    char register_index_1 = arg1 - 0x41;
    registers[6]++;
    char register_index_2 = fgetc(f) - 0x41;
    registers[6]++;
    // Get the values from the registers
    char value_1 = registers[get_right_register_index(register_index_1)];
    char value_2 = registers[get_right_register_index(register_index_2)];
    
    registers[get_right_register_index(register_index_1)] = value_2;
   
    printf("%d : mov $%d, $%d\n", temp_adress, get_right_register_index(register_index_1), get_right_register_index(register_index_2));

    temp_adress = registers[6];
}

void vm_pop(FILE* f, char byte){
   registers[6]++;
    char register_index_1 = fgetc(f) - 0x41;
    registers[6]++;
    printf("%d : pop $%d\n", temp_adress, register_index_1);
    temp_adress = registers[6];
}

void vm_push(FILE* f, char byte){
   registers[6]++;
    char register_index_1 = fgetc(f) - 0x41;
    registers[6]++;
    printf("%d : push $%d\n", temp_adress, register_index_1);
    temp_adress = registers[6];
}

void vm_push_n_sized_memory_to_stack(FILE* f, char byte){
    unsigned char arg1 = fgetc(f);
    unsigned char temp_byte;
    printf("%d : push_n_sized_memory_to_stack %d\n", temp_adress, arg1);
    registers[6]++;
    fgetc(f);
    registers[6]++;
    fgetc(f);
    registers[6]++;
    fgetc(f);
    registers[6]++;
    printf("{");
    for (int i = 0; i < arg1; i++)
    {
        printf("\'0x%x\', ",fgetc(f));
        registers[6]++;
    }
    printf("}\n");
   // printf("R6 : %d\n", registers[6]);
    
    temp_adress = registers[6];
}
