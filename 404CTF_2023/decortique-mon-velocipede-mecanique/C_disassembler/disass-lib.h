#ifndef DISASS_H
#define DISASS_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// Array of VM registers
extern int registers[8];

//Adress of IP before instruction (used to display the number of the instruction)
extern int temp_adress;

// Function prototypes
int search_for_function(FILE *f, char *string);
int get_right_register_index(int x);
void vm_jump(FILE *f,  char byte);
void vm_movc(FILE *f,  char byte);
void vm_puts(FILE *f,  char byte);
void vm_getline(FILE *f,  char byte);
void vm_andl(FILE *f,  char byte);
void vm_call(FILE *f,  char byte);
void vm_ret(FILE *f,  char byte);
void vm_add(FILE *f,  char byte);
void vm_sub(FILE *f,  char byte);
void vm_exit_routine(FILE *f,  char byte);
void vm_inc_ic(FILE *f,  char byte);
void vm_fopen(FILE *f,  char byte);
void vm_mov(FILE *f,  char byte);
void vm_pop(FILE *f,  char byte);
void vm_push(FILE *f,  char byte);
void vm_xorl(FILE *f,  char byte);
void vm_push_n_sized_memory_to_stack(FILE *f,  char byte);


#endif