#include "disass-lib.h"

int registers[8];

int temp_adress;

int main() {
    
    for (int i = 0; i < 8; i++)
    {
        registers[i] = 0;
    }

    // Search for the main function
    FILE *f = fopen("reverse_my_vm.vmr", "rb");
    if (f == NULL) {
       printf("Error opening file\n");
       return 1;
    }

    int main_function_address = search_for_function(f, "main");
    if (main_function_address == -1) {
       printf("Could not find main function\n");
       return 1;
    }

    printf("\nRegisters $0, $1, $2, $3, $4, $5 are storage. $6 is IC, $7 is SP\n\n");

    // Set the instruction counter to the main function address
    registers[6] = main_function_address;

    // Parse the file byte by byte
    while (1) {
       char byte = fgetc(f);
       if (byte == EOF) {
          break;
       }

       switch (byte) {
          case 0x21:
             vm_jump(f, byte);
             break;
          case 0x23:
             vm_movc(f, byte);
             break;
          case 0x24:
             vm_puts(f, byte);
             break;
          case 0x25:
             vm_getline(f, byte);
             break;
          case 0x26:
             vm_andl(f, byte);
             break;
          case 0x28:
             vm_call(f, byte);
             break;
          case 0x29:
             vm_ret(f, byte);
             break;
          case 0x2b:
             vm_add(f, byte);
             break;
          case 0x2d:
             vm_sub(f, byte);
             break;
          case 0x2e:
             vm_exit_routine(f, byte);
             break;
          case 0x20:
             vm_inc_ic(f, byte);
             break;
          case 0x2f:
             vm_fopen(f, byte);
             break;
          case 0x3a:
             vm_mov(f, byte);
             break;
          case 0x3c:
             vm_pop(f, byte);
             break;
          case 0x3e:
             vm_push(f, byte);
             break;
          case 0x5e:
             vm_xorl(f, byte);
             break;
          case 0x7c:
             vm_push_n_sized_memory_to_stack(f, byte);
             break;
          default:
             printf("Unknown byte %x\n", byte);
             break;
       }
    }

    fclose(f);

    return 0;
}
