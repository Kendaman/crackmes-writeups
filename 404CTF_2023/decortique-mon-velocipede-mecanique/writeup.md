# CHALLENGE DESCRIPTION

This challenge comes from the 404CTF (Organized by Hackademint and the DGSE). 
The name is "décortique-mon-vélocipède-mécanique". It was a challenge that was described as hard.
The challenge is constitued of an ELF binary named "VM" and from another file named "reverse_my_vm.vmr".
The VM binary uses the .vmr file to launch the program contained in it.

# PRELIMINARY ANALYSIS

When launching the VM executable with the .vmr file, we can notice that the program asks for a key. If the wrong key is entered, the program outputs "function 'check_key' not found". This can point us to the fact that the key that is entered by the user is then used by the binary to generate another piece of code (A packing technique). The .vmr file contains the string "check_key", as well as "main", and other strings relative to the outputs of the program, thus confirming that the .vmr file is some sort of program.

# STATIC ANALYSIS

After opening the VM executable in Ghidra's decompiler, and searching for a little bit, a switch statement can be found. This switch statement iterates over the different bytes of the .vmr file which indicates that those are intructions of a custom assembly language.
Thus, it is possible to reverse the instruction set of the virtual machine. 

Seventeen different instructions where found.

The following syntax is used : 

    - $x represents the register x of the VM
    - # represents a numerical constant
    - %arg represent an argument for an instruction

| Instruction | Byte | Usage |
|---|---|---|
| jnz %arg | 0x21 | Jumps to vm_instruction_pointer+%arg if the register $0 is not zero  |
| movc $1, #a | 0x23 | Move the constant #a to the register $1 |
| puts | 0x24 | Prints a message situated in memory (at $0 + vm_memory_space_start) |
| getline | 0x25 | Get input from the user, and push it to the VM's stack |
| andl $1, $2 | 0x26 | Perform a logical and between register $1 and $2 |
| call %len %function_name| 0x28 | Search for the string %function_name in the memory space of the VM, and puts the VM's instruction pointer there |
| ret | 0x29 | Return from a function call |
| add $1, $2 | 0x2b | Add two registers |
| sub $1, $2 | 0x2d | Substract two registers |
| exit | 0x2e | Exit the program |
| inc_ic | 0x20 | Increase the instruction counter of the VM by one|
| fopen | 0x2f | Opens a file where the string is at $0 + vm_memory_space_start |
| mov $1, $2 | 0x3a | Move the value contained in $2 to $1 |
| pop $1 | 0x3c | Pop the value of the VM's stack in $1 |
| push $1 | 0x3e | Push the value of $1 to the VM's stack|
| xorl $1, $2 | 0x5e | Perform a logical xor between $1 and $2|
| push_n_bytes %n %bytes | 0x7c | Push %n bytes after the instruction to the VM's stack|

In an effort of efficiency (and for fun), i developped a disassembly tool for this custom instruction set.
I developped two versions (One in C, and the other in python). Some instructions were untested.

# DYNAMIC ANALYSIS

The disassembler outputs the following code : 
```
Registers $0, $1, $2, $3, $4, $5 are storage. $6 is IC, $7 is SP

7 : push_n_sized_memory_to_stack 23
{'0x50', '0x6c', '0x65', '0x61', '0x73', '0x65', '0x20', '0x65', '0x6e', '0x74', '0x65', '0x72', '0x20', '0x74', '0x68', '0x65', '0x20', '0x6b', '0x65', '0x79', '0x3a', '0x20', '0x0', }
34 : mov $0, $7
37 : puts $0
38 : movc $1, #0x0
42 : getline
43 : mov $3, $7
46 : push_n_sized_memory_to_stack 176
{'0x63', '0x3d', '0x21', '0x11', '0x22', '0x52', '0x19', '0x33', '0x9', '0x51', '0x3b', '0x7b', '0x3b', '0x2b', '0x72', '0x6c', '0x62', '0x7a', '0x2d', '0xe', '0x6b', '0x11', '0x17', '0x2', '0x16', '0x51', '0x30', '0x59', '0x33', '0x59', '0x17', '0x4c', '0x12', '0x55', '0x31', '0xa', '0x24', '0x5e', '0x16', '0x9', '0x58', '0x14', '0x42', '0x43', '0x6', '0x62', '0x56', '0x4f', '0x20', '0x34', '0x43', '0x79', '0x47', '0x14', '0x4e', '0x2a', '0x1e', '0x36', '0x42', '0x79', '0x47', '0x3b', '0x72', '0x10', '0x7a', '0x34', '0x42', '0x79', '0x3', '0x35', '0x25', '0x44', '0x46', '0x46', '0x5a', '0x42', '0x3', '0x66', '0x7a', '0x36', '0x7c', '0x67', '0x7e', '0x23', '0x44', '0x5a', '0x59', '0x34', '0x7e', '0x6c', '0x0', '0x6f', '0x7d', '0x74', '0x21', '0x4f', '0x23', '0x32', '0x42', '0x79', '0x47', '0x12', '0x30', '0x6d', '0x62', '0x34', '0x42', '0x5a', '0x4', '0x35', '0x72', '0x6c', '0x62', '0x8', '0x6', '0x27', '0x3', '0x77', '0x4c', '0x28', '0x49', '0x67', '0x1', '0x54', '0x6', '0x73', '0x53', '0x9e', '0x9d', '0xcb', '0xbd', '0x43', '0x14', '0x74', '0xe', '0x74', '0x62', '0x34', '0x42', '0x3a', '0x28', '0x5f', '0x15', '0x1e', '0x3', '0x40', '0x31', '0x58', '0x67', '0x68', '0x1d', '0x19', '0x10', '0x14', '0x24', '0x15', '0x26', '0x56', '0x52', '0x5', '0x11', '0xe', '0x62', '0x43', '0x6', '0x62', '0x56', '0x32', '0x23', '0x75', '0x6c', '0x59', '0x14', '0x66', '0x33', '0x2b', }
226 : xorl $4, $4
229 : movc $0, #0x2c
233 : mov $1, $7
236 : mov $7, $3
239 : add $7, $4
242 : pop $2
244 : mov $7, $1
247 : pop $1
249 : xorl $1, $2
252 : push $1
254 : movc $5, #0x4
258 : add $7, $5
261 : add $4, $5
264 : andl $4, $5
267 : movc $5, #0x1
271 : sub $0, $5
274 : jnz 233
279 : movc $5, #0x0
283 : sub $7, $5
286 : call check_key
```

As we can see, the program asks for a user input, and uses the first 8 bytes of this input to unpack 176 bytes that are pushed on the VM's stack. Because when entering a wrong key, the program outputs that it does not find the "check_key" function, it is likely that our input is used to put the "check_key" string on the stack. The "call" routine used  by the VM is peculiar in the way that to search a function, it first search for the byte 0x1, then the length of the name of the function, then the functions name, ended by the byte 0x2. In memory the function "check_key" should then be represented as follow : ``` '\x1' '\x9' 'c' 'h' 'e' 'c' 'k' '_' 'k' 'e' 'y' '\x2' ```

Moreover, knowing the properties of the xor operation (a ^ b = c <=> a ^ c = b), that the first 8 bytes that are pushed to the stack are : ```'0x63', '0x3d', '0x21', '0x11', '0x22', '0x52', '0x19', '0x33'```, and that the packing process was the the following : ```password ^ ('\x1' '\x9' 'c' 'h' 'e' 'c' 'k' '_') = first_8_bytes_on_stack```, we can just xor the following vectors to obtain the password :

``` ('\x1' '\x9' 'c' 'h' 'e' 'c' 'k' '_') ^ '0x63', '0x3d', '0x21', '0x11', '0x22', '0x52', '0x19', '0x33' ```


The resulting operation give the bytes of the password : ```b4ByG1rl```

The program then asks us a for a key, and generates a flag based on that key.
The 404CTF flags being of the following format : ```404CTF{1ns3rt_FuNny_l33T_phr4se}```

The disassembler can be used to disassemble the unpacked code, and find how the flag is generated by the second key. 