# CHALLENGE DESCRIPTION

This challenge comes from the starhackademint 2023 CTF (https://star.hackademint.org). It was described as medium.
The challenge consist of a single, stripped, ELF binary. That asks for a password.

# STATIC ANALYSIS

When opening the file with Ghidra's decompiler, the following code is given :

```
undefined8 FUN_00401186(void)

{
  size_t strlen_str;
  long lVar1;
  ulong uVar2;
  undefined8 *puVar3;
  undefined8 *puVar4;
  byte bVar5;
  int local_48 [4];
  char input [32];
  undefined8 *local_18;
  uint local_c;
  
  bVar5 = 0;
  printf("Password:");
  __isoc99_scanf(&DAT_0040200e,input);
  strlen_str = strlen(input);
  if (strlen_str != 0x14) {
    puts("No.");
                    /* WARNING: Subroutine does not return */
    exit(0);
  }
  lVar1 = ptrace(PTRACE_TRACEME,0,0);
  local_48[0] = (int)lVar1 + 0xa1;
  lVar1 = ptrace(PTRACE_TRACEME,0,0);
  local_48[1] = (int)lVar1 + 0xb7;
  lVar1 = ptrace(PTRACE_TRACEME,0,0);
  local_48[2] = (int)lVar1 + 0xe0;
  lVar1 = ptrace(PTRACE_TRACEME,0,0);
  local_48[3] = (int)lVar1 + 0x22;
  local_18 = (undefined8 *)0x0;
  for (local_c = 0; local_c < DAT_004041ac; local_c = local_c + 1) {
    *(byte *)((long)&DAT_00404080 + (long)(int)local_c) =
         (byte)local_48[(int)local_c % 4] ^ *(byte *)((long)&DAT_00404080 + (long)(int)local_c);
  }
  local_18 = (undefined8 *)mmap((void *)0x0,300,7,0x22,-1,0);
  *local_18 = DAT_00404080;
  *(undefined8 *)((long)local_18 + 0x124) = DAT_004041a4;
  lVar1 = (long)local_18 - (long)(undefined8 *)((ulong)(local_18 + 1) & 0xfffffffffffffff8);
  puVar3 = (undefined8 *)((long)&DAT_00404080 - lVar1);
  puVar4 = (undefined8 *)((ulong)(local_18 + 1) & 0xfffffffffffffff8);
  for (uVar2 = (ulong)((int)lVar1 + 300U >> 3); uVar2 != 0; uVar2 = uVar2 - 1) {
    *puVar4 = *puVar3;
    puVar3 = puVar3 + (ulong)bVar5 * -2 + 1;
    puVar4 = puVar4 + (ulong)bVar5 * -2 + 1;
  }
  (*(code *)local_18)(input);
  return 0;
} 
```

This is the central code of the program.  As we can see, ptrace is called three times, tracing the current process. Making the process able to know if it is being debugged. The values returned by ptrace being stored in the local_48 table, those values being used in a for loop that operates on part of the binary indicate that this might be a packing technique. 

Moreover, the two final lines, where the value at the pointer local_18 is called, shows that code is indeed unpacked if ptrace return good values.

# DYNAMIC ANALYSIS

When run in a debugger, the program produces a SIGILL. The program taking a wrong course (not being able to call the unpacked code), when being debugged (The values are unpacked with the values returned by ptrace ), it is possible with the debugger to replace the values returned by ptrace by the good ones. 

Let me explain. When ptrace is called on a process that is not being traced, it returns 0. But if it is already traced, it returns -1. So, when the program is not debugged, the first ptrace returns 0, then the two others return -1. Thus, when the program is being debugged, the three ptrace call return -1, thus wrongly unpacking the code in the binary.

# UNPACKING

After fiddling with the debugger by changing the values returned by ptrace, we can see that the code executed is the following : 
```
   xor    rax,rax
   xor    rbx,rbx
   mov    al,BYTE PTR [rdi]
   mov    bl,0x53 // S
   cmp    al,bl
   jne    0x7ffff7ffa110
   mov    al,BYTE PTR [rdi+0x1]
   add    bl,0x21 //t
   cmp    al,bl
   jne    0x7ffff7ffa110
   mov    al,BYTE PTR [rdi+0x2]
   sub    bl,0x13 //a
   cmp    al,bl
   jne    0x7ffff7ffa110
   mov    al,BYTE PTR [rdi+0x3]
   add    bl,0x11 //r
   cmp    al,bl
   jne    0x7ffff7ffa110
   mov    al,BYTE PTR [rdi+0x4]
   add    bl,0x9 //{
   cmp    al,bl
   jne    0x7ffff7ffa110
   mov    al,BYTE PTR [rdi+0x5]
   sub    bl,0x2b //P
   cmp    al,bl
   jne    0x7ffff7ffa110
   mov    al,BYTE PTR [rdi+0x6]
   sub    bl,0x1c //4
   cmp    al,bl
   jne    0x7ffff7ffa110
   mov    al,BYTE PTR [rdi+0x7]
   add    bl,0x2f //c
   cmp    al,bl
   jne    0x7ffff7ffa110
   mov    al,BYTE PTR [rdi+0x8]
   add    bl,0x8 //k
   cmp    al,bl
   jne    0x7ffff7ffa110
   mov    al,BYTE PTR [rdi+0x9]
   sub    bl,0xc //_
   cmp    al,bl
   jne    0x7ffff7ffa110
   mov    al,BYTE PTR [rdi+0xa]
   add    bl,0x11 //  p
   cmp    al,bl
   jne    0x7ffff7ffa110
   mov    al,BYTE PTR [rdi+0xb]
   sub    bl,0x3c //4
   cmp    al,bl
   jne    0x7ffff7ffa110
   mov    al,BYTE PTR [rdi+0xc]
   add    bl,0xf //C
   cmp    al,bl
   jne    0x7ffff7ffa110
   mov    al,BYTE PTR [rdi+0xd]
   add    bl,0x28 //k
   cmp    al,bl
   jne    0x7ffff7ffa110
   mov    al,BYTE PTR [rdi+0xe]
   sub    bl,0xc //_
   cmp    al,bl
   jne    0x7ffff7ffa110
   mov    al,BYTE PTR [rdi+0xf]
   sub    bl,0xf //P
   cmp    al,bl
   jne    0x7ffff7ffa110
   mov    al,BYTE PTR [rdi+0x10]
   sub    bl,0x1c //4
   cmp    al,bl
   jne    0x7ffff7ffa110
   mov    al,BYTE PTR [rdi+0x11]
   add    bl,0x2f //c
   cmp    al,bl
   jne    0x7ffff7ffa110
   mov    al,BYTE PTR [rdi+0x12]
   sub    bl,0x18 //K
   cmp    al,bl
   jne    0x7ffff7ffa110
   mov    al,BYTE PTR [rdi+0x13]
   add    bl,0x32//}
   cmp    al,bl
   jne    0x7ffff7ffa110
   mov    esi,0xa214747
   push   rsi
   mov    rsi,rsp
   mov    eax,0x1
   mov    edi,0x1
   mov    edx,0x4
   syscall 
   pop    rdi
   ret    
   mov    esi,0xa2e6f4e
   push   rsi
   mov    rsi,rsp
   mov    eax,0x1
   mov    edi,0x1
   mov    edx,0x4
   syscall 
   pop    rdi
   ret    
```

We can clearly see that the bytes of the user input ($rdi+x) are compared to other bytes. Thus it is possible to get the wanted flag : Star{P4ck_p4Ck_P4cK}