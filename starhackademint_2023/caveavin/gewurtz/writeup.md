# CHALLENGE DESCRIPTION

 This challenge comes from the starhackademint 2023 CTF (https://star.hackademint.org). It was described as really easy.
The challenge consist of a single, non-stripped, ELF binary. That asks for a password.

# STATIC ANALYSIS

By running the `strings` command on the binary, the following output is given :

```
/lib64/ld-linux-x86-64.so.2
exit
__libc_start_main
printf
strcmp
libc.so.6
GLIBC_2.2.5
GLIBC_2.34
__gmon_start__
PTE1
H=(@@
Usage : ./caveavin1 [PASSWORD]
Star{C0mp4ra1s0n_D3_stR1ngS}
Too Bad...
;*3$"
GCC: (GNU) 13.2.0
crt1.o
__abi_tag
crtstuff.c
deregister_tm_clones
__do_global_dtors_aux
completed.0
__do_global_dtors_aux_fini_array_entry
frame_dummy
__frame_dummy_init_array_entry
source.c
__FRAME_END__
_DYNAMIC
__GNU_EH_FRAME_HDR
_GLOBAL_OFFSET_TABLE_
__libc_start_main@GLIBC_2.34
_edata
_fini
printf@GLIBC_2.2.5
__data_start
strcmp@GLIBC_2.2.5
__gmon_start__
__dso_handle
_IO_stdin_used
_end
_dl_relocate_static_pie
__bss_start
main
exit@GLIBC_2.2.5
__TMC_END__
_init
.symtab
.strtab
.shstrtab
.interp
.note.gnu.property
.note.ABI-tag
.gnu.hash
.dynsym
.dynstr
.gnu.version
.gnu.version_r
.rela.dyn
.rela.plt
.init
.text
.fini
.rodata
.eh_frame_hdr
.eh_frame
.init_array
.fini_array
.dynamic
.got
.got.plt
.data
.bss
.comment
```

As we can see, there is a string that looks like a flag.

# DYNAMIC ANALYSIS

When loading the program inside of a debugger (I am using GDB GEF). We can see that the password is compared to another string using the `strcmp` function. This string is : `Star{C0mp4ra1s0n_D3_stR1ngS}`

Thus the flag has been found.
