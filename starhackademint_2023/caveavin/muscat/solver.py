from z3 import *

# Create 17 symbolic variables (each representing a character)
characters = [BitVec(f"char_{i}", 8) for i in range(24)]

matrix = [[0,1,0,1,1,1,1,1,1,1,1,0,1,0,1,1,1,1,1,0,0,1,1,0],
[1,0,1,1,1,1,1,0,1,1,0,1,1,1,0,0,1,1,0,0,0,0,1,0], 
[1,1,0,0,1,1,1,1,1,0,1,0,1,1,1,0,1,0,1,0,1,0,0,1], 
[0,1,0,1,1,1,0,0,0,0,1,1,1,1,1,0,1,1,1,0,0,1,1,1], 
[1,0,1,0,0,1,1,1,1,0,1,1,1,1,1,0,0,1,1,0,1,0,1,0], 
[1,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,1,0,0,0,1,1], 
[1,1,0,1,1,1,0,1,0,1,0,1,1,1,1,1,1,0,1,1,1,0,1,1],
[0,1,0,1,1,1,1,1,1,1,1,0,0,0,1,0,1,1,1,1,1,0,1,1], 
[1,1,1,1,1,0,1,0,1,1,1,1,1,1,1,1,1,1,0,1,1,1,0,0], 
[1,1,0,1,0,1,0,0,1,1,1,1,1,0,0,1,1,1,1,1,1,1,0,0], 
[0,1,1,0,1,1,1,1,1,0,1,1,1,1,1,0,1,1,1,1,1,1,0,1], 
[1,1,1,1,0,1,0,1,1,1,1,1,1,1,1,1,0,0,1,1,1,1,1,0], 
[0,1,1,1,0,1,0,1,1,1,1,1,1,1,1,1,0,1,1,1,1,0,1,1], 
[0,1,1,1,1,1,1,1,1,0,0,1,0,1,1,0,1,1,0,1,1,1,1,1], 
[1,0,1,1,1,1,0,1,1,1,0,0,1,0,0,0,1,0,1,1,1,0,1,0], 
[1,1,1,1,1,1,1,0,1,1,1,1,1,1,0,1,1,1,0,1,1,0,1,0],
[0,0,0,0,0,1,1,1,1,1,1,1,0,1,1,0,1,0,1,1,1,1,0,0], 
[0,0,1,1,1,0,0,1,1,1,0,1,0,1,0,0,1,1,0,0,1,1,1,1],
[0,1,0,1,1,1,0,1,1,0,1,0,1,1,1,1,0,1,0,1,1,1,0,0],
[1,1,1,1,1,1,1,0,1,1,0,1,0,0,1,1,1,1,0,1,1,1,1,1],
[1,1,1,1,0,1,0,1,1,1,1,0,1,1,0,0,1,0,0,0,0,1,1,0], 
[0,1,1,0,1,1,1,1,1,1,0,0,0,1,0,1,0,0,1,1,0,1,1,1], 
[1,1,1,1,1,1,1,0,0,1,0,0,0,1,1,0,0,1,0,1,0,1,1,0],
[1,0,1,0,1,1,0,0,0,1,0,1,0,0,1,0,1,1,1,1,0,0,1,1], 
[1,1,1,1,0,0,1,0,1,1,0,0,1,1,1,0,1,1,1,0,1,0,1,1], 
[1,1,1,1,1,1,1,1,1,1,0,1,1,0,1,0,1,1,0,1,1,1,1,1], 
[0,1,0,0,0,0,1,0,0,0,1,0,0,1,1,1,0,1,0,0,1,1,1,1], 
[0,0,0,1,1,1,0,1,1,1,1,1,0,0,1,0,1,0,1,1,1,1,1,1]] 

constraints = [28, 120, 40, 52, 72, 99, 118, 31, 54, 18, 23, 100, 103, 14, 34, 111, 7, 57, 106, 40, 34, 89, 100, 84, 102, 116, 100, 82]


def filter_bitvectors_by_indices(bitvectors, indices):
    result = []
    for i, bv in enumerate(bitvectors):
        if indices[i] == 1:
            result.append(bv)
    return result

def list_to_string(char_list):
    # Join the characters in the list into a single string
    result_string = ''.join(char_list)
    return result_string


# Create a Z3 solver instance
solver = Solver()

# Add constraints to ensure characters are valid ASCII (0-127)
for char in characters:
    solver.add(And(char >= 33, char <= 126))
    solver.add(char != 96)

solver.add(characters[0] == 83)
solver.add(characters[1] == 116)
solver.add(characters[2] == 97)
solver.add(characters[3] == 114)
solver.add(characters[4] == 123)
solver.add(characters[23] == 125)

def find_solution_for_line(bitvectors, indices, constraint):
    buffer_xor = filter_bitvectors_by_indices(bitvectors, indices)
    xor_result = buffer_xor[0]
    for i in range(1, len(buffer_xor)):
        xor_result = xor_result ^ buffer_xor[i]

    solver.add(xor_result == constraint)

for i in range(len(matrix)):
    find_solution_for_line(characters, matrix[i], constraints[i])


# Check if there's a solution
if solver.check() == sat:
    model = solver.model()
    characters_values = [model[char].as_long() for char in characters]
    print("Solution found:")
    print("Characters:", list_to_string([chr(char) for char in characters_values]))
else:
    print("No solution found.")
