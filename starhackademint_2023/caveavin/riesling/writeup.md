# CHALLENGE DESCRIPTION

This challenge comes from the starhackademint 2023 CTF (https://star.hackademint.org). It was described as easy.
The challenge consist of a single, stripped, ELF binary. That asks for a password.

# STATIC ANALYSIS

After opening the file in Ghidra's decompiler, the following code is given :

```
void FUN_00401146(int param_1,long param_2)

{
  size_t sVar1;
  
  if (param_1 < 2) {
    printf("Usage : ./caveavin2 [PASSWORD]");
                    /* WARNING: Subroutine does not return */
    exit(1);
  }
  sVar1 = strlen(*(char **)(param_2 + 8));
  if (sVar1 != 0x18) {
    printf("Too Bad...");
                    /* WARNING: Subroutine does not return */
    exit(0);
  }
  if (*(char *)(*(long *)(param_2 + 8) + 3) != 'r') {
    printf("Too Bad...");
                    /* WARNING: Subroutine does not return */
    exit(0);
  }
  if (*(char *)(*(long *)(param_2 + 8) + 5) != '4') {
    printf("Too Bad...");
                    /* WARNING: Subroutine does not return */
    exit(0);
  }
  if (*(char *)(*(long *)(param_2 + 8) + 0x15) != 'g') {
    printf("Too Bad...");
                    /* WARNING: Subroutine does not return */
    exit(0);
  }
  if (*(char *)(*(long *)(param_2 + 8) + 0x14) != 'n') {
    printf("Too Bad...");
                    /* WARNING: Subroutine does not return */
    exit(0);
  }
  if (*(char *)(*(long *)(param_2 + 8) + 0x17) != '}') {
    printf("Too Bad...");
                    /* WARNING: Subroutine does not return */
    exit(0);
  }
  if (*(char *)(*(long *)(param_2 + 8) + 4) != '{') {
    printf("Too Bad...");
                    /* WARNING: Subroutine does not return */
    exit(0);
  }
  if (*(char *)(*(long *)(param_2 + 8) + 0xc) != '3') {
    printf("Too Bad...");
                    /* WARNING: Subroutine does not return */
    exit(0);
  }
  if (*(char *)(*(long *)(param_2 + 8) + 10) != '_') {
    printf("Too Bad...");
                    /* WARNING: Subroutine does not return */
    exit(0);
  }
  if (**(char **)(param_2 + 8) != 'S') {
    printf("Too Bad...");
                    /* WARNING: Subroutine does not return */
    exit(0);
  }
  if (*(char *)(*(long *)(param_2 + 8) + 0x12) != 'L') {
    printf("Too Bad...");
                    /* WARNING: Subroutine does not return */
    exit(0);
  }
  if (*(char *)(*(long *)(param_2 + 8) + 2) != 'a') {
    printf("Too Bad...");
                    /* WARNING: Subroutine does not return */
    exit(0);
  }
  if (*(char *)(*(long *)(param_2 + 8) + 9) != '1') {
    printf("Too Bad...");
                    /* WARNING: Subroutine does not return */
    exit(0);
  }
  if (*(char *)(*(long *)(param_2 + 8) + 0xd) != '_') {
    printf("Too Bad...");
                    /* WARNING: Subroutine does not return */
    exit(0);
  }
  if (*(char *)(*(long *)(param_2 + 8) + 0x16) != '!') {
    printf("Too Bad...");
                    /* WARNING: Subroutine does not return */
    exit(0);
  }
  if (*(char *)(*(long *)(param_2 + 8) + 0x11) != 'S') {
    printf("Too Bad...");
                    /* WARNING: Subroutine does not return */
    exit(0);
  }
  if (*(char *)(*(long *)(param_2 + 8) + 8) != '0') {
    printf("Too Bad...");
                    /* WARNING: Subroutine does not return */
    exit(0);
  }
  if (*(char *)(*(long *)(param_2 + 8) + 0x10) != '3') {
    printf("Too Bad...");
                    /* WARNING: Subroutine does not return */
    exit(0);
  }
  if (*(char *)(*(long *)(param_2 + 8) + 0x13) != '1') {
    printf("Too Bad...");
                    /* WARNING: Subroutine does not return */
    exit(0);
  }
  if (*(char *)(*(long *)(param_2 + 8) + 0xe) != 'r') {
    printf("Too Bad...");
                    /* WARNING: Subroutine does not return */
    exit(0);
  }
  if (*(char *)(*(long *)(param_2 + 8) + 0xb) != 'l') {
    printf("Too Bad...");
                    /* WARNING: Subroutine does not return */
    exit(0);
  }
  if (*(char *)(*(long *)(param_2 + 8) + 1) != 't') {
    printf("Too Bad...");
                    /* WARNING: Subroutine does not return */
    exit(0);
  }
  if (*(char *)(*(long *)(param_2 + 8) + 0xf) != '1') {
    printf("Too Bad...");
                    /* WARNING: Subroutine does not return */
    exit(0);
  }
  if (*(char *)(*(long *)(param_2 + 8) + 7) != 'm') {
    printf("Too Bad...");
                    /* WARNING: Subroutine does not return */
    exit(0);
  }
  if (*(char *)(*(long *)(param_2 + 8) + 6) != '_') {
    printf("Too Bad...");
                    /* WARNING: Subroutine does not return */
    exit(0);
  }
  printf("GG!");
                    /* WARNING: Subroutine does not return */
  exit(0);
}
```

As we can see, each character of the inputed password (*(char *)(*(long *)(param_2 + 8))) is compared, byte by byte, to other characters.
Thus, the flag is : `Star{4_m01_l3_r13SL1ng !}`

