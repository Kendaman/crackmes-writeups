# A WRITE UP IS COMING !

This challenge was of difficulty "Medium", and relied on the fact that the C `rand()` function  is pseudo-random. It used those values to validate a file passed as input. I had to automate the resolution of this crackme to generate the flag.
